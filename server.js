const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const brandRouter = require('./src/brands/brandRouter');
const modelRouter = require('./src/models/modelRouter');
const Config = require('./config.js');

const logger = Config.Logger;
const app = express();
app.use(bodyParser.urlencoded({
  extended: true,
}));
app.use(bodyParser.json());
app.use(cors());

app.use((req, res, next) => {
  if (req.url !== '/favicon.ico') {
    logger.info(`Requested Page: ${req.url}`);
    next();
  }
});
app.use('/brands', brandRouter.router);
app.use('/models', modelRouter.router);
app.listen(3001, () => {
  logger.info('MySql Server running on port 3001');
});
