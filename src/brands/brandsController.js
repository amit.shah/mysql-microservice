const mysql = require('mysql2/promise');
const Config = require('../../config');

const logger = Config.Logger;
function endConnection(error) {
  logger.error(`There is error: ${error}`);
  process.exit();
}
async function connectToDatabase() {
  let connection;
  try {
    connection = await mysql.createConnection({
      host: Config.sqlconfig.host,
      user: Config.sqlconfig.username,
      password: Config.sqlconfig.password,
      database: Config.sqlconfig.database,
    });
    logger.info('SQL Connection established');
  } catch (error) {
    logger.error(error);
    endConnection(error);
  }
  return connection;
}
async function getAllBrandData() {
  logger.info('Controller: Fetching all brand data');
  let connection;
  let result;
  try {
    connection = await connectToDatabase();
    const query = 'SELECT  * from brand_data;';
    const [row] = await connection.query(query);
    result = row;
    connection.end();
  } catch (error) {
    logger.error(error);
    endConnection(error);
  } finally {
    logger.info('SQL Connection End');
    connection.end();
  }
  return result;
}
async function getBrandData(brand) {
  logger.info(`Controller: Fetching brand: ${brand} data`);
  let connection;
  let result;
  try {
    connection = await connectToDatabase();
    const query = `SELECT * from brand_data where name = '${brand}';`;
    const row = await connection.query(query);
    connection.end();
    result = row;
  } catch (error) {
    endConnection(error);
  } finally {
    logger.info('SQL Connection End');
    connection.end();
  }
  return result;
}
async function updateBrandTable(brand, brandDetail) {
  logger.info(`Controlelr: Updating brand data of ${brand}`);
  let connection;
  try {
    connection = await connectToDatabase();
    const query = 'update brand_data set name = ?,owner = ?,founded = ?, headquarter = ?, revenue = ?, website = ?, employee = ?, about = ?, history = ?, finance = ?, img = ?  where name = ?;';
    await connection.execute(query,
      [brandDetail.name,
        brandDetail.owner,
        brandDetail.founded,
        brandDetail.headquarter,
        brandDetail.revenue,
        brandDetail.website,
        brandDetail.employee,
        brandDetail.about,
        brandDetail.history,
        brandDetail.finance,
        brandDetail.image,
        brand]);
    return true;
  } catch (error) {
    endConnection(error);
    return false;
  } finally {
    logger.info('SQL Connection End');
    connection.end();
  }
}
async function addNewBrand(brandDetail) {
  logger.info(`Controller: Adding new brand i.e ${brandDetail.name}`);
  let connection;
  try {
    connection = await connectToDatabase();
    const query = 'insert into brand_data(name,owner,founded,headquarter,revenue,website,employee,about,history,finance,img) values (?,?,?,?,?,?,?,?,?,?,?);';
    await connection.execute(query,
      [brandDetail.name,
        brandDetail.owner,
        brandDetail.founded,
        brandDetail.headquarter,
        brandDetail.revenue,
        brandDetail.website,
        brandDetail.employee,
        brandDetail.about,
        brandDetail.history,
        brandDetail.finance,
        brandDetail.image]);
    return true;
  } catch (error) {
    endConnection(error);
    return false;
  } finally {
    logger.info('SQL Connection End');
    connection.end();
  }
}
async function deleteBrand(brandName) {
  logger.info(`Controller: Deleting  brand i.e ${brandName}`);
  let connection;
  try {
    connection = await connectToDatabase();
    const query = 'DELETE from brand_data where name = ?';
    await connection.execute(query, [brandName]);
    return true;
  } catch (error) {
    endConnection(error);
    return false;
  } finally {
    logger.info('SQL Connection End');
    connection.end();
  }
}
module.exports = {
  getAllBrandData,
  getBrandData,
  updateBrandTable,
  addNewBrand,
  deleteBrand,
};
