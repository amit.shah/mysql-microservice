const express = require('express');
const brandData = require('./brandsController');
const Config = require('../../config');

const router = express.Router();
const logger = Config.Logger;
router.get('/getData', async (req, res) => {
  logger.info('Router: Request to get all Brand Data');
  try {
    const brandDetail = await brandData.getAllBrandData();
    res.json(brandDetail);
  } catch (error) {
    logger.log(error);
  }
});
router.get('/getData/:brand', async (req, res) => {
  const brandName = (req.params.brand).toLowerCase();
  logger.info(`Router: Request to get brand: ${brandName} data`);
  const brandDetail = await brandData.getBrandData(brandName);
  res.json(brandDetail);
});
router.post('/edit/:brand', async (req, res) => {
  const brandName = req.params.brand;
  logger.info(`Router: Request to edit brand: ${brandName} data`);
  const updateBrandTable = await brandData.updateBrandTable(brandName, req.body);
  if (updateBrandTable) {
    res.json({ status: 'success' });
  } else {
    res.json({ status: 'failure' });
  }
});
router.post('/addNew', async (req, res) => {
  const addBrand = await brandData.addNewBrand(req.body);
  logger.info('Router: Request to add new brand data');
  if (addBrand) {
    res.json({ status: 'success' });
  } else {
    res.json({ status: 'failure' });
  }
});
router.delete('/delete/:brand', async (req, res) => {
  const brandName = req.params.brand;
  logger.info(`Router: Request to delete brand: ${brandName} data`);
  const deleteStatus = await brandData.deleteBrand(brandName);
  if (deleteStatus) {
    res.json({ status: 'success' });
  } else {
    res.json({ status: 'failure' });
  }
});
module.exports = {
  router,
};
