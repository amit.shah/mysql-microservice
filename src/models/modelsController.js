const mysql = require('mysql2/promise');
const Config = require('../../config');

const logger = Config.Logger;
async function connectToDatabase() {
  try {
    const connection = await mysql.createConnection({
      host: Config.sqlconfig.host,
      user: Config.sqlconfig.username,
      password: Config.sqlconfig.password,
      database: Config.sqlconfig.database,
    });
    logger.info('Connection established');
    return connection;
  } catch (error) {
    logger.error(`There is error: ${error}`);
    process.exit();
  }
  return 'connection Failed';
}
async function performQuery(query) {
  let connection;
  try {
    connection = await connectToDatabase();
    const [row] = await connection.query(query);
    return row;
  } catch (error) {
    logger.error(`There is error: ${error}`);
    process.exit();
  } finally {
    connection.end();
  }
  return 'Performing query Failed';
}
async function getAllModelData() {
  logger.info('Controller: Fetching all model data');
  const query = 'SELECT * from models_data;';
  const result = await performQuery(query);
  return result;
}
async function getModelData(model) {
  logger.info(`Controller: Fetching Model: ${model} data`);
  const query = `SELECT * from models_data where name = '${model}' ;`;
  const result = await performQuery(query);
  return result;
}
async function updateModelTable(modelName, modelData) {
  logger.info(`Controller: Fetching Model: ${modelName} data`);
  let connection;
  try {
    connection = await connectToDatabase();
    const query = 'update models_data set name = ?, release_date = ?, network = ?, dimension= ?, weight = ? ,display = ? , size = ? ,resolution = ?, os = ?, chipset = ?, cpu = ? , gpu = ? ,image= ?,company = ? where name = ?;';
    await connection.execute(query,
      [modelData.name,
        +modelData.release_date,
        modelData.network,
        modelData.dimension,
        +modelData.weight,
        modelData.display,
        modelData.size,
        modelData.resolution,
        modelData.os,
        modelData.chipset,
        modelData.cpu,
        modelData.gpu,
        modelData.image,
        modelData.company,
        modelName]);
    return true;
  } catch (error) {
    logger.error(`There is error: ${error}`);
    return false;
  } finally {
    logger.info('connection end');
    connection.end();
  }
}
// eslint-disable-next-line consistent-return
async function addNewModel(modelData) {
  logger.info(`Controller: Adding model :${modelData.name}`);
  let connection;
  try {
    connection = await connectToDatabase();
    const findBrandID = `select company_id from brand_data where name = '${modelData.company}';`;
    const result = await connection.query(findBrandID);
    const brandID = result[0][0].company_id;
    if (findBrandID) {
      const query = 'insert into models_data(name, release_date, network, dimension, weight, display, size, resolution, os, chipset, cpu, gpu,image, company, company_id) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);';
      await connection.execute(query,
        [modelData.name,
          +modelData.release_date,
          modelData.network,
          modelData.dimension,
          +modelData.weight,
          modelData.display,
          modelData.size,
          modelData.resolution,
          modelData.os,
          modelData.chipset,
          modelData.cpu,
          modelData.gpu,
          modelData.image,
          modelData.company,
          brandID]);
      return true;
    }
  } catch (error) {
    logger.error(`There is error: ${error}`);
    return false;
  } finally {
    connection.end();
  }
}
async function deleteModel(modelName) {
  logger.info(`Controller: Deleting brand: ${modelName} data`);
  let connection;
  try {
    connection = await connectToDatabase();
    const query = 'DELETE from models_data where name = ?';
    await connection.execute(query, [modelName]);
    return true;
  } catch (error) {
    logger.error(`There is error: ${error}`);
    return false;
  } finally {
    logger.info('SQL Connection End');
    connection.end();
  }
}
module.exports = {
  getAllModelData,
  getModelData,
  updateModelTable,
  addNewModel,
  deleteModel,
};
