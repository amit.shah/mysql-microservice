const express = require('express');
const modelData = require('./modelsController');
const Config = require('../../config');

const logger = Config.Logger;
const router = express.Router();

router.get('/getData', async (req, res) => {
  logger.info('Router: Request to get all Model Data');
  const modelDetail = await modelData.getAllModelData();
  res.json(modelDetail);
});
router.get('/getData/:model', async (req, res) => {
  const modelName = (req.params.model).toLowerCase();
  logger.info(`Router: Request to get Model: ${modelName} data`);
  const modelDetail = await modelData.getModelData(modelName);
  res.json(modelDetail);
});
router.post('/edit/:model', async (req, res) => {
  const modelName = req.params.model;
  logger.info(`Router: Request to edit model: ${modelName} data`);
  const updateStatus = await modelData.updateModelTable(modelName, req.body);
  if (updateStatus) {
    res.send({ status: 'success' });
  } else {
    res.send({ status: 'failure' });
  }
});
router.post('/addNewModel', async (req, res) => {
  logger.info('Router: Request to add new Model');
  const addStatus = await modelData.addNewModel(req.body);
  if (addStatus) {
    res.send({ status: 'success' });
  } else {
    res.send({ status: 'failure' });
  }
});
router.delete('/delete/:model', async (req, res) => {
  const modelName = req.params.model;
  logger.info(`Router: Request to delete brand: ${modelName} data`);
  const deleteStatus = await modelData.deleteModel(modelName);
  if (deleteStatus) {
    res.json({ status: 'success' });
  } else {
    res.json({ status: 'failure' });
  }
});
module.exports = {
  router,
};
