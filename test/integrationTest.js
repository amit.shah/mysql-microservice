/* eslint-disable no-useless-escape */
/* eslint-disable no-undef */
/* eslint-disable prefer-destructuring */
const route = require('supertest');
const assert = require('chai').assert;

const server = 'http://13.233.96.127:3001';

describe('Brand Get Data', () => {
  it('should return all brand data', (done) => {
    route(server)
      .get('/brands/getData')
      .expect((res) => {
        assert.equal(res.body[0].company_id, 1);
      })
      .expect('Content-Type', /json/, done);
  });
});

describe('Samsung Brand Get Data', () => {
  it('should return all data of Samsung', (done) => {
    route(server)
      .get('/brands/getData/Samsung')
      .expect((res) => {
        assert.equal(res.body[0][0].name, 'Samsung');
      })
      .expect('Content-Type', /json/, done);
  });
});

describe('Adding a brand', () => {
  it('Should add a new brand', (done) => {
    const brandDetail = {
      name: 'ASD',
      owner: 'XCS',
      founded: '2008',
      headquarter: 'Delhi',
      revenue: '$10 millions',
      website: 'www.ASD.com',
      employee: '789',
      about: 'ASD',
      history: 'ASD',
      finance: 'ASD',
      image: 'images/ASD.jpg',
    };
    route(server)
      .post('/brands/addNew/')
      .send(brandDetail)
      .expect((res) => {
        assert.equal(res.body.status, 'success');
      })
      .expect('Content-Type', 'application/json; charset=utf-8', done);
  });
});

describe('Deleting a brand', () => {
  it('Should delete a  brand', (done) => {
    route(server)
      .delete('/brands/delete/ASD')
      .expect((res) => {
        assert.equal(res.body.status, 'success');
      })
      .expect('Content-Type', 'application/json; charset=utf-8', done);
  });
});

describe('Model Get Data', () => {
  it('should return all model data', (done) => {
    route(server)
      .get('/models/getData')
      .expect((res) => {
        assert.equal(res.body[0].company_id, 3);
      })
      .expect('Content-Type', /json/, done);
  });
});
describe('Note 9 Brand Get Data', () => {
  it('should return all data of S8', (done) => {
    route(server)
      .get('/models/getData/Note-9')
      .expect((res) => {
        assert.equal(res.body[0].name, 'Note-9');
      })
      .expect('Content-Type', /json/, done);
  });
});

describe('Adding a Model', () => {
  it('Should add a new Model', (done) => {
    const modelDetail = {
      name: 'OP2',
      release_date: '2019',
      network: 'GSM',
      dimension: '12*10',
      weight: '140',
      display: 'LCD',
      size: '5.5',
      resolution: '400 ppi',
      os: 'Android 7',
      chipset: 'SD 880',
      cpu: 'cpu',
      gpu: 'adreno',
      image: 'images/OP2.jpg',
      company: 'Samsung',
      api_index: 'P20',
    };
    route(server)
      .post('/models/addNewModel/')
      .send(modelDetail)
      .expect((res) => {
        assert.equal(res.body.status, 'success');
      })
      .expect('Content-Type', 'application/json; charset=utf-8', done);
  });
});
describe('Deleting a model', () => {
  it('Should delete a model', (done) => {
    route(server)
      .delete('/models/delete/OP2')
      .expect((res) => {
        assert.equal(res.body.status, 'success');
      })
      .expect('Content-Type', 'application/json; charset=utf-8', done);
  });
});
