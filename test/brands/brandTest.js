/* eslint-disable no-undef */
/* eslint-disable prefer-destructuring */
const expect = require('chai').expect;
const brandsController = require('../../src/brands/brandsController');

describe('Getting a brand founded date', () => {
  it('Samsung founded date should be equal to 1923', async () => {
    const result = await brandsController.getBrandData('Samsung');
    await expect(result[0][0].founded).to.equal('1923');
  });
});
describe('Getting all brand data and checking a single brand data', () => {
  it('Samsung employee should be ', async () => {
    const result = await brandsController.getAllBrandData();
    await expect(result[2].employee).to.equal('15,222');
  });
});
describe('Adding a brand', () => {
  it('Add a new brand in brand_data table', async () => {
    const brandDetail = {
      name: 'ASD',
      owner: 'XCS',
      founded: '2008',
      headquarter: 'Delhi',
      revenue: '$10 millions',
      website: 'www.ASD.com',
      employee: '789',
      about: 'ASD',
      history: 'ASD',
      finance: 'ASD',
      image: 'images/ASD.jpg',
    };
    const result = await brandsController.addNewBrand(brandDetail);
    await expect(result).to.equal(true);
  });
});
describe('Updating a brand detail', () => {
  it('Updating ASD brand detail', async () => {
    const brandDetail = {
      name: 'ASD',
      owner: 'NEWXCS',
      founded: '2008',
      headquarter: 'Delhi',
      revenue: '$10 millions',
      website: 'www.ASD.com',
      employee: '789',
      about: 'ASD',
      history: 'ASD',
      finance: 'ASD',
      image: 'images/ASD.jpg',
    };
    const result = await brandsController.updateBrandTable('ASD', brandDetail);
    await expect(result).to.equal(true);
  });
});
describe('Deleting a brand', () => {
  it('Delete ASD brand from brand_data table', async () => {
    const result = await brandsController.deleteBrand('ASD');
    await expect(result).to.equal(true);
  });
});
