/* eslint-disable no-undef */
/* eslint-disable prefer-destructuring */
const expect = require('chai').expect;
const modelController = require('../../src/models/modelsController');

describe('Getting a model size', () => {
  it('Note 9 A1 weight should be equal to 140', async () => {
    const result = await modelController.getModelData('Note-9');
    await expect(result[0].weight).to.equal(189);
  });
});

describe('Getting all models data and checking a model data from it', () => {
  it('A1 release date should be 2016', async () => {
    const result = await modelController.getAllModelData();
    await expect(result[0].name).to.equal('Pocco-F1');
  });
});
describe('Adding a new model', () => {
  it('Add new OP2 model detail', async () => {
    const modelDetail = {
      name: 'OP2',
      release_date: '2019',
      network: 'GSM',
      dimension: '12*10',
      weight: '140',
      display: 'LCD',
      size: '5.5',
      resolution: '400 ppi',
      os: 'Android 7',
      chipset: 'SD 880',
      cpu: 'cpu',
      gpu: 'adreno',
      image: 'images/OP2.jpg',
      company: 'Samsung',
      api_index: 'P20',
    };
    const result = await modelController.addNewModel(modelDetail);
    await expect(result).to.equal(true);
  });
});
describe('Updating a model detail', () => {
  it('Updating ABC brand detail', async () => {
    const modelDetail = {
      name: 'OP2',
      release_date: '2018',
      network: 'GSM',
      dimension: '12*10',
      weight: '140',
      display: 'LCD',
      size: '5.5',
      resolution: '400 ppi',
      os: 'Android 7',
      chipset: 'SD 880',
      cpu: 'cpu',
      gpu: 'adreno',
      image: 'images/OP2.jpg',
      company: 'Samsung',
      api_index: 'P20',
    };
    const result = await modelController.updateModelTable('OP2', modelDetail);
    await expect(result).to.equal(true);
  });
});
describe('Deleting a model', () => {
  it('Delete OP2 model from models_data table', async () => {
    const result = await modelController.deleteModel('OP2');
    await expect(result).to.equal(true);
  });
});
