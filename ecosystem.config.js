module.exports = {
  apps: [{
    name: 'Mysql microservice',
    script: 'server.js',
  }],

  deploy: {
    production: {
      user: 'ubuntu',
      key: '../aws/mobileManiaKey.pem',
      host: '13.233.96.127',
      ref: 'origin/master',
      repo: 'git@gitlab.com:amit.shah/mysql-microservice.git',
      path: '/home/ubuntu/mysql-microservice/',
      'post-deploy': './node_modules/.bin/pm2 reload ecosystem.config.js --env production',
    },
  },
};
