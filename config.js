const winston = require('winston');

const sqlconfig = (process.env.type === 'development')
  ? {
    host: 'localhost',
    username: 'webProject',
    password: '12',
    database: 'mobileMania',
  }
  : {
    host: 'dbinstance.cbcdptvda1cs.ap-south-1.rds.amazonaws.com',
    username: 'amit',
    password: 'qwerty123',
    database: 'mobileMania',
  };

const Logger = winston.createLogger({
  level: 'info',
  format: winston.format.simple(),
  transports: [
    new winston.transports.Console(),
  ],
});

module.exports = {
  Logger,
  sqlconfig,
};
